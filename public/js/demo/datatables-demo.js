// Call the dataTables jQuery plugin
$(document).ready(function() {
  $('#dataTable').DataTable({

    "columnDefs": [
      { "searchable": false, "targets": [0,2] },
      { "width": "10%", "targets": 0 }
    ]

  }

  );

});
