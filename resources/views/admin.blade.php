<x-admin>
    @section('content')
     <h3>Admin Panel</h3>
        @if(Session::has('updateMessage'))
            <div class="alert alert-success">{{Session::get('updateMessage')}}</div>
        @endif
     <form action="{{route('updateAdmin', '1')}}" method="post">
        @csrf
        @method('PATCH')

@foreach(\App\Models\Admin::all() as $Columns)
             <div class="form-group">
                 <label for="First column">Url Address</label>
                 <input type="text" class="form-control" name="address" value="{{$Columns->address}}">
             </div>

     <div class="form-group">
    <label for="First column">First column Name</label>
    <input type="text" class="form-control" placeholder="{{$Columns->firstColumn}}" name="firstColumn" value="{{$Columns->firstColumn}}">
    Width<input type="text" class="form-control" placeholder="{{$Columns->firstColumnWidth}}" name="firstColumnWidth" value="{{$Columns->firstColumnWidth}}">
     </div>

     <div class="form-group">
    <label for="Second column">Second column Name</label>
    <input type="text" class="form-control" placeholder="{{$Columns->secondColumn}}" name="secondColumn" value="{{$Columns->secondColumn}}">
    Width<input type="text" class="form-control" placeholder="{{$Columns->secondColumnWidth}}" name="secondColumnWidth" value="{{$Columns->secondColumnWidth}}">
     </div>

     <div class="form-group">
    <label for="Third column">Third column Name</label>
    <input type="text" class="form-control" placeholder="{{$Columns->thirdColumn}}" name="thirdColumn" value="{{$Columns->thirdColumn}}">
    Width<input type="text" class="form-control" placeholder="{{$Columns->thirdColumnWidth}}" name="thirdColumnWidth" value="{{$Columns->thirdColumnWidth}}">
     </div>
     <button type="submit" class="btn btn-primary">Submit</button>
     </form>
@endforeach








    @endsection
</x-admin>
