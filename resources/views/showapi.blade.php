<x-admin>

    @section('content')
 @foreach(\App\Models\Admin::all() as $Columns)
    <table class="table" id="dataTable" data-order='[[ 1 , "asc" ]]'>
    <thead>
    <tr>

        <th scope="col">{{$Columns->firstColumn}}</th>
        <th scope="col">{{$Columns->secondColumn}}</th>
        <th scope="col">{{$Columns->thirdColumn}}</th>


    </tr>
    </thead>

    <tbody>
        @foreach($columnsList as $data)

    <tr>

        <td style="width: {{$Columns->firstColumnWidth}}% ">{{$data[0]}}</td>
        <td style="width: {{$Columns->secondColumnWidth}}% ">{{$data[1]}}</td>
        <td style="width: {{$Columns->thirdColumnWidth}}% ">{{$data[2]}}</td>

        @endforeach
    </tr>
        @endforeach

    @endsection
    </tbody>



        @section('scripts')
        <!-- Bootstrap core JavaScript-->
<script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

<!-- Core plugin JavaScript-->
<script src="{{asset('vendor/jquery-easing/jquery.easing.min.js')}}"></script>

<!-- Custom scripts for all pages-->
<script src="{{asset('js/sb-admin-2.min.js')}}"></script>
        <!-- Page level plugins -->
            <script src="{{asset('vendor/datatables/jquery.dataTables.min.js')}}"></script>
            <script src="{{asset('vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

            <!-- Page level custom scripts -->
            <script src="{{asset('js/demo/datatables-demo.js')}}"></script>
        @endsection
</x-admin>
