<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\api;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;

class ApiController extends Controller
{
    //get data from URL - api mode
    public function getDataFromUrl() {
        $url = Admin::find(1);
        //$url =  "http://www.thecocktaildb.com/api/json/v1/1/search.php?s=margarita";
        $json = json_decode(file_get_contents($url->address), true);
        return $json;

    }

    //select columns
    public function selectColumns(){
        $json = $this->getdataFromUrl();
        $columnsList = [];
        foreach($json['drinks'] as $columns) {
            $selectedColumns = array(
              $column['col1'] = $columns['idDrink'] ,
              $column['col2'] = $columns['strDrink'],
              $column['col3'] = $columns['strTags']
            ) ;
            $columnsList[] = $selectedColumns;
        }
        return  $columnsList;
    }

    //select data types
     public function selectDataType(){
         $json = $this->selectColumns();
         $columnsList =[];
         foreach ($json as $values) {
            settype($values[0], "integer");
            settype($values[1], "string");
            settype($values[2], "string");
            $selectedColumns = array(
             $column['col1'] = $values[0] ,
             $column['col2'] = $values[1],
             $column['col3'] = $values[2]
            );
             $columnsList[] = $selectedColumns;
         }
         $columnsList = new Paginator($columnsList, 4);
         return view('showapi', compact('columnsList'));
     }


        //Master method load other methods
        public function showData() {
        return $this->selectDataType();
        }




}
