<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller
{
    public function index() {
        return view('admin');
    }

    public function update(Admin $admin) {
        $values = request()->validate([
            'address'             => 'required',
            'firstColumn'         => 'required',
            'firstColumnWidth'    => 'nullable ',
            'secondColumn'        => 'required',
            'secondColumnWidth'   => 'nullable ',
            'thirdColumn'         => 'required',
            'thirdColumnWidth'    => 'nullable ',

        ]);
        $admin->address             = $values['address'];
        $admin->firstColumn         = $values['firstColumn'];
        $admin->firstColumnWidth    = $values['firstColumnWidth'];
        $admin->secondColumn        = $values['secondColumn'];
        $admin->secondColumnWidth   = $values['secondColumnWidth'];
        $admin->thirdColumn         = $values['thirdColumn'];
        $admin->thirdColumnWidth    = $values['thirdColumnWidth'];
        $admin->save();

        Session::flash('updateMessage', 'Front Page Updated Successfully!');
        return redirect()->route('indexAdmin');
    }
}
